﻿using DataCleanupTool.JsonUtils;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DataCleanupTool.Models
{
    public class TagsOnlyItem
    {
        public TagsOnlyItem() { }
        public TagsOnlyItem(string objectId)
        {
            ObjectId = objectId;
        }
        public TagsOnlyItem(IList<string> tags)
        {
            Tags = tags;
        }
        public TagsOnlyItem(IList<string> tags, string objectId)
        {
            Tags = tags;
            ObjectId = objectId;
        }

        [JsonProperty("_tags")]
        public IList<string> Tags { get; set; }

        [JsonProperty("objectID")]
        public string ObjectId { get; set; }

        [JsonProperty("_tagsWithContext"), JsonIgnoreSerializationAttribute]
        public IDictionary<string, IList<string>> TagsWithContext { get; set; }
    }
}