﻿using Algolia.Search.Clients;
using Algolia.Search.Iterators;
using Algolia.Search.Models.Common;
using DataCleanupTool.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StackExchange.Redis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;

namespace DataCleanupTool
{
    class Runner
    {
        private SearchIndex AlgoliaIndex;
        private ConnectionMultiplexer RedisConnection;
        private IDatabase RedisRegionA;
        private int BadObjectCount = 0;
        private int ObjectsNotInRedis = 0;
        private int ObjectsMissingProductListInRedis = 0;
        private Hashtable ProductListMap = new Hashtable();

        private readonly string RedisTagPrefix = ConfigurationManager.AppSettings["RedisCacheKeyPrefix"];

        public async void RunAsync()
        {
            Console.WriteLine("Initializing Algolia, Redis, and ProductListMap...");
            AlgoliaIndex = InitializeAlgoliaIndex();
            RedisConnection = InitializeRedisRegion();
            RedisRegionA = RedisConnection.GetDatabase(1);
            GenerateProductListMapFromCsv();
            Console.WriteLine("Algolia, Redis, and ProductListMap initialized. Starting browse...");
            
            await BrowseAlgoliaAndCrossReferenceRedis();

            Console.WriteLine($"Done browsing index");
            Console.WriteLine($"Objects with corrupted Product Lists in Redis Counted: {BadObjectCount}");
            Console.WriteLine($"Objects with no Product Lists in Redis Counted: {ObjectsMissingProductListInRedis}");
            Console.WriteLine($"Objects non-existent in Redis Counted: {ObjectsNotInRedis}");

            Console.ReadKey();
        }

        private void GenerateProductListMapFromCsv()
        {
            var fullPath = Path.GetFullPath("./ProductLists.csv");
            Console.WriteLine(fullPath);
            if (File.Exists(fullPath)) 
            {
                Console.WriteLine("File Exists.");
                using (var reader = new StreamReader(fullPath))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var values = line.Split(',');
                        var dimensionId = values[1];
                        var cmsId = values[0];
                        ProductListMap.Add(dimensionId, cmsId);
                    }
                }
            }
            else
            {
                Console.WriteLine("File does not exist.");
                Console.WriteLine(Directory.GetFiles(fullPath));
                return;
            }
            Console.WriteLine(ProductListMap.ToString());
        }

        private async Task BrowseAlgoliaAndCrossReferenceRedis()
        {
            var algoliaQuery = new BrowseIndexQuery();
            var algoliaResults = AlgoliaIndex.Browse<JObject>(algoliaQuery);
            await ProcessAlgoliaResults(algoliaResults);
        }

        private Task ProcessAlgoliaResults(IndexIterator<JObject> algoliaResults)
        {
            var count = 0;
            foreach (var algoliaResult in algoliaResults)
            {
                count++;
                var objectID = algoliaResult["objectID"].ToString();
                var redisResult = GetRedisValueForSku(objectID);

                if (string.IsNullOrWhiteSpace(redisResult))
                {
                    ObjectsNotInRedis++;
                    continue;
                }

                var redisValue = JsonConvert.DeserializeObject<TagsOnlyItem>(redisResult);
                CrossReferenceRedisResultTags(redisValue);

                if(count % 1000 == 0)
                {
                    Console.WriteLine($"Count: {count}");
                }
                if (count >= 50000)
                    break;
            }
            Console.WriteLine($"Hits: {count}");
            return Task.CompletedTask;
        }

        private void CrossReferenceRedisResultTags(TagsOnlyItem redisValue)
        {
            if(!redisValue.TagsWithContext.ContainsKey("ProductList"))
            {
                ObjectsMissingProductListInRedis++;
                return;
            }

            var listOfDimensionIds = redisValue.TagsWithContext["ProductList"];
            foreach (var dimensionId in listOfDimensionIds)
            {
                if (!ProductListMap.ContainsKey(dimensionId))
                {
                    BadObjectCount++;
                    return;
                }
            }
        }

        private string GetRedisValueForSku(string objectID)
        {
            return RedisRegionA.StringGet($"{RedisTagPrefix}:{objectID}");
        }

        private SearchIndex InitializeAlgoliaIndex()
        {
            var algoliaAppID = ConfigurationManager.AppSettings["AlgoliaAppId"];
            var algoliaApiKey = ConfigurationManager.AppSettings["AlgoliaApiKey"];
            var algoliaIndexName = ConfigurationManager.AppSettings["AlgoliaIndexName"];
            var client = new SearchClient(algoliaAppID, algoliaApiKey);
            var index = client.InitIndex(algoliaIndexName);
            return index;
        }

        private ConnectionMultiplexer InitializeRedisRegion()
        {
            var redisConnectionString = ConfigurationManager.ConnectionStrings["RedisAPrimary"].ToString();
            return ConnectionMultiplexer.Connect(redisConnectionString);
        }

        
    }
}
